
##Git global setup##
git config --global user.name "jhpark"
git config --global user.email "endless103@gmail.com"

##Create a new repository##
git clone https://gitlab.com/endless103/ejm.git
cd ejm
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

##Push an existing folder##
cd existing_folder
git init
git remote add origin https://gitlab.com/endless103/ejm.git
git add .
git commit -m "Initial commit"
git push -u origin master

##Push an existing Git repository##
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/endless103/ejm.git
git push -u origin --all
git push -u origin --tags
